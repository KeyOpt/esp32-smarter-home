#include <Arduino.h>
#include "meas.h"  //NOLINT(build/include_subdir)
#include <SPIFFS.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <HTTPUpdate.h>
#include <WiFiSettings.h>
#include <PubSubClient.h>

/*
To Upload to the server via curl:
 curl -v -F filename=firmware.bin -F file=@.pio/build/ezsbc/firmware.bin -H 'Content-Type: multipart/form-data' http://192.168.10.253:5000/upload
*/
#define VERSION "v0.0.16"
#define HOST "ESP32SmarterHome"
const char* urlBase = "http://192.168.10.253:5000/update";
WiFiClient ESPhttpUpdate;

#define RLED 16
#define GLED 17
#define BLED 18
#define BLED2 19

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
Meas meas;
int serial_number = 0;

void callback(char* topic, byte *payload, unsigned int length){
  Serial.println("------new message from broker -------");
}

int connect_mqtt_server()
{
  while (!mqttClient.connected()){
    printf("Attempting MQTT connection...");

    String clientId = "BlogStack-";
    clientId += String(random(0xFFFF), HEX);

    if (mqttClient.connect(clientId.c_str())) {
      printf("Connected!\n");
      return 0;
    }
  }
  return 1;
}

/***************************************************/
void check_for_updates(void)
{
  String checkUrl = String( urlBase);
  checkUrl.concat( "?ver=" + String(VERSION) );
  checkUrl.concat( "&dev=" + String(HOST) );

  log_i("Checking for updates at URL: %s With MAC: %s",String( checkUrl ).c_str(),WiFi.macAddress().c_str());

  WiFiClient client;
  t_httpUpdate_return ret = httpUpdate.update( client, checkUrl );

  switch (ret) {
    default:
    case HTTP_UPDATE_FAILED:
      log_e("HTTP_UPDATE_FAILD Error (%s): %s", String(httpUpdate.getLastError()).c_str(), httpUpdate.getLastErrorString().c_str());
      break;
    case HTTP_UPDATE_NO_UPDATES:
      log_i("HTTP_UPDATE_NO_UPDATES");
      break;
    case HTTP_UPDATE_OK:
      log_i("HTTP_UPDATE_OK");
      break;
    }
}

void turnOnLED(uint8_t led)
{
  pinMode(led, OUTPUT);
  GPIO.out_w1tc = ((uint32_t) 1 << led);
}

void turnOffLED(uint8_t led)
{
  pinMode(led, OUTPUT);
  GPIO.out_w1ts = ((uint32_t) 1 << led);
}

void turnOffAllLED()
{
  turnOffLED(RLED);
  turnOffLED(GLED);
  turnOffLED(BLED);
  turnOffLED(BLED2);
}

void setup() {
  // put your setup code here, to run once:
  meas = Meas();
  String greeting = meas.get_hello() + "\n";
  printf("%s", greeting.c_str());

  SPIFFS.begin(true);  // On first run, will format after failing to mount
  //To reset stored credentials, run SPIFFS.format()
  //SPIFFS.format();
  // Use stored credentials to connect to your WiFi access point.
  // If no credentials are stored or if the access point is out of reach,
  // an access point will be started with a captive portal to configure WiFi.
  serial_number = WiFiSettings.integer("Serial Number", 0, 65535, 0);
  String host = WiFiSettings.string("MQTT Server", "192.168.10.253");
  int    port = WiFiSettings.integer("MQTT Port", 0, 65535, 1883);
  
  WiFiSettings.connect(true, 30);
  //WiFiSettings.portal();
  printf("Wifi Connect done %lu\n", millis());
  printf("Serial: %d\n", serial_number);
  printf("MQTT Host:%s:%d\n", host.c_str(), port);

  mqttClient.setServer(host.c_str(), port);
  mqttClient.setCallback(callback);
  int result = connect_mqtt_server();
  while (result){
    delay(1000);
    result = connect_mqtt_server();
  }
}

void deep_sleep(uint16_t minutes){
  turnOffAllLED();
  //Subtract millis to more accurately hit a reproduceable posting rate
  // This handles how long we have been awake already.
  uint64_t time = minutes * 60 * 1000 * 1000;
  if (millis() < time){
    // Ideally suited for cases where we wake up, perform measurement, then go to sleep
    esp_sleep_enable_timer_wakeup((uint64_t)time - millis());
  }else{
    // If for some reason we have been running for a long time, do not do the above optimization.
    esp_sleep_enable_timer_wakeup((uint64_t)((uint64_t) minutes * 60 * 1000 * (uint64_t)1000));
  }
  
  esp_deep_sleep_start();
}

void loop() {
  char reading[120];
  char topic[120];
  // put your main code here, to run repeatedly:
  printf("Mag: %d, time: %lu\n", meas.get_hall(), millis());

  snprintf(topic, sizeof(topic), "blogstack/sensor%d/hall", serial_number);
  snprintf(reading, sizeof(reading), "%d", meas.get_hall());
  mqttClient.publish(topic,reading, true);

  check_for_updates();
  deep_sleep(1);
}