#ifndef LIB_MEASUREMENTS_MEAS_H_
#define LIB_MEASUREMENTS_MEAS_H_

#include <Arduino.h>

class Meas {
    public:
        Meas();
        String get_hello();
        int get_hall();
    private:
        String data;
};

#endif  // LIB_MEASUREMENTS_MEAS_H_