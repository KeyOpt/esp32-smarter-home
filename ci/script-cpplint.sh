python -m pip install cpplint

cd repo
cpplint --root=. --output=junit --filter=-whitespace,-legal/copyright,-readability/multiline_comment --linelength=180 --recursive src/* include/* lib/* 2> ../cpplint/cpplint.xml